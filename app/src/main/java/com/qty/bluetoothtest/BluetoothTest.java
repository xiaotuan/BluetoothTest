package com.qty.bluetoothtest;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class BluetoothTest extends Activity {

    private static final String TAG = "BluetoothTest";
    private static final int MSG_BLUETOOTH_DEVICE_FOUND = 0;
    private static final int MSG_REDISCOVERY = 1;
    private static final int MSG_DISCOVERY_STARTED = 2;
    private static final int MSG_DISCOVERY_FINISHED = 3;
    private static final int MSG_STATE_CHANGED = 4;
    private static final int DELAYED_REDISCOVERY = 10000;
    private static final int DISCOVERY_TIMEOUT = 60000;

    private BluetoothAdapter mBluetoothAdapter;
    private TextView mBluetoothStateTv;
    private ListView mBluetoothLv;
    private TextView mEmptyView;
    private BluetoothListAdapter mAdapter;
    private ArrayList<BluetoothDevice> mDevices;

    private boolean mLastBluetoothEnabled = false;
    private boolean mNeedOpenBluetoothWhenOff = false;
    private boolean mIsFirstScan = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_test);

        BluetoothManager bm = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        mBluetoothAdapter = bm.getAdapter();
        mLastBluetoothEnabled = mBluetoothAdapter.isEnabled();
        mDevices = new ArrayList<BluetoothDevice>();

        mBluetoothStateTv = (TextView) findViewById(R.id.bluetooth_state);
        mBluetoothLv = (ListView) findViewById(R.id.bluetooth_list);

        initEmptyView();
        mAdapter = new BluetoothListAdapter(this, mDevices);
        mBluetoothLv.setAdapter(mAdapter);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.gravity = Gravity.CENTER;
        ((ViewGroup) mBluetoothLv.getParent()).addView(mEmptyView, lp);
        mBluetoothLv.setEmptyView(mEmptyView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()...");
        mIsFirstScan = true;
        openBluetooth();
        updateBluetoothState();
        registerBluetoothReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()...");
        unregisterBluetoothReceiver();
        cancelDiscovery();
        closeBluetooth();
    }

    private void initEmptyView() {
        mEmptyView = new TextView(this);
        mEmptyView.setText(R.string.no_bluetooth_available_title);
        mEmptyView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        mEmptyView.setGravity(Gravity.CENTER);
    }

    private void updateBluetoothState() {
        int state = mBluetoothAdapter.getState();
        Log.d(TAG, "updateBluetoothState=>state: " + state);
        switch (state) {
            case BluetoothAdapter.STATE_TURNING_ON:
                mBluetoothStateTv.setText(R.string.bluetooth_turning_on_state_text);
                break;

            case BluetoothAdapter.STATE_ON:
                mBluetoothStateTv.setText(R.string.bluetooth_on_state_text);
                startDiscovery();
                break;

            case BluetoothAdapter.STATE_TURNING_OFF:
                mBluetoothStateTv.setText(R.string.bluetooth_turning_off_state_text);
                break;

            case BluetoothAdapter.STATE_OFF:
                mBluetoothStateTv.setText(R.string.bluetooth_off_state_text);
                if (mNeedOpenBluetoothWhenOff) {
                    openBluetooth();
                    mNeedOpenBluetoothWhenOff = false;
                }
                break;

            default:
                mBluetoothStateTv.setText(R.string.bluetooth_unknow_state_text);
                break;
        }
    }

    private void openBluetooth() {
        if (!mBluetoothAdapter.isEnabled()) {
            if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_OFF) {
                mBluetoothAdapter.enable();
            } else {
                mNeedOpenBluetoothWhenOff = true;
            }
        }
    }

    private void closeBluetooth() {
        if (!mLastBluetoothEnabled) {
            mBluetoothAdapter.disable();
        }
    }

    private void startDiscovery() {
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
        //mBluetoothAdapter.setDiscoverableTimeout(DISCOVERY_TIMEOUT);
        mBluetoothAdapter.startDiscovery();
    }

    private void cancelDiscovery() {
        mBluetoothAdapter.cancelDiscovery();
    }

    private void addBluetoothDevice(BluetoothDevice device) {
        mDevices.add(device);
        mAdapter.setDeviceList(mDevices);
    }

    private void registerBluetoothReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mBluetoothReceiver, filter);
    }

    private void unregisterBluetoothReceiver() {
        unregisterReceiver(mBluetoothReceiver);
    }

    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive=>action: " + action);
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                mHandler.sendEmptyMessage(MSG_DISCOVERY_FINISHED);
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                mHandler.sendEmptyMessage(MSG_DISCOVERY_STARTED);
            } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                mHandler.sendEmptyMessage(MSG_STATE_CHANGED);
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                String name = intent.getStringExtra(BluetoothDevice.EXTRA_NAME);
                Log.d(TAG, "onReceive=>found device: " + name);
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Message msg = new Message();
                msg.obj = device;
                msg.what = MSG_BLUETOOTH_DEVICE_FOUND;
                mHandler.sendMessage(msg);
            }
        }
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_BLUETOOTH_DEVICE_FOUND:
                    BluetoothDevice device = (BluetoothDevice) msg.obj;
                    if (mIsFirstScan){
                        addBluetoothDevice(device);
                    } else{
                        mDevices.add(device);
                    }
                    break;

                case MSG_REDISCOVERY:
                    startDiscovery();
                    break;

                case MSG_DISCOVERY_STARTED:
                    mEmptyView.setText(R.string.scanning_bluetooth_devices_title);
                    mDevices.clear();
                    break;

                case MSG_DISCOVERY_FINISHED:
                    if (!mIsFirstScan) {
                        mAdapter.setDeviceList(mDevices);
                    }
                    mEmptyView.setText(R.string.no_bluetooth_available_title);
                    mHandler.sendEmptyMessageDelayed(MSG_REDISCOVERY, DELAYED_REDISCOVERY);
                    mIsFirstScan = false;
                    break;

                case MSG_STATE_CHANGED:
                    updateBluetoothState();
                    break;
            }
        }
    };

    private class BluetoothListAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mInflater;
        private ArrayList<BluetoothDevice> mList;

        public BluetoothListAdapter(Context context, ArrayList<BluetoothDevice> list) {
            mContext = context;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mList = list;
        }

        private void setDeviceList(ArrayList<BluetoothDevice> list) {
            mList = null;
            mList = list;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return (mList == null ? 0 : mList.size());
        }

        @Override
        public BluetoothDevice getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.bluetooth_item, parent, false);
                holder = new ViewHolder();
                holder.mName = (TextView) convertView.findViewById(R.id.bluetooth_name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            BluetoothDevice device = getItem(position);
            String name = device.getName();
            if (TextUtils.isEmpty(name)) {
                name = device.getAddress();
            }
            holder.mName.setText(name);
            return convertView;
        }

        class ViewHolder {
            TextView mName;
        }
    }

}
